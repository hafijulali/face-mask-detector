from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import to_categorical

def augment_label_data(data, labels):
	lb = LabelBinarizer()
	labels = lb.fit_transform(labels)
	labels = to_categorical(labels)

	(trainX, testX, trainY, testY) = train_test_split(data, labels,
		test_size=0.20, stratify=labels, random_state=42)

	aug = ImageDataGenerator(
		rotation_range=20,
		zoom_range=0.15,
		width_shift_range=0.2,
		height_shift_range=0.2,
		shear_range=0.15,
		horizontal_flip=True,
		fill_mode="nearest")
	return trainX, testX, trainY, testY,  aug, lb